package id.teknologi.orderfood.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import id.teknologi.orderfood.R
import id.teknologi.orderfood.model.MakananModel

class AdapterMakanan(val context : Context, val listModel : ArrayList<MakananModel>, val callback : AdapterMakananCallback)
    : RecyclerView.Adapter<AdapterMakanan.ViewHolderMakanan>() {


    // inisiasi view holder
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolderMakanan {
        val inflater = LayoutInflater.from(context)
        // view yang akan digunakan adalah item_makanan
        val view = inflater.inflate(R.layout.item_makanan, viewGroup, false)

        return ViewHolderMakanan(view)
    }

    // jumlah dari item yang akan ditampilkan
    override fun getItemCount(): Int {
        return listModel.size
    }

    // mengatur tampilan view pada urutan ke-idx
    override fun onBindViewHolder(viewHolder: ViewHolderMakanan, idx: Int) {
        viewHolder.onBind(listModel[idx])
    }

    inner class ViewHolderMakanan(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val textNama : TextView
        val textDeskripsi : TextView
        val textHarga : TextView
        val imageMakanan : ImageView
        val textTotal : TextView

        init {
            // binding / menghubungkan variable dengan view item_makanan.xml
            textNama = itemView.findViewById(R.id.tv_nama_makanan)
            textDeskripsi = itemView.findViewById(R.id.tv_deskripsi_makanan)
            textHarga = itemView.findViewById(R.id.tv_harga_makanan)
            imageMakanan = itemView.findViewById(R.id.img_makanan)
            textTotal = itemView.findViewById(R.id.tv_total_makanan)

            // atur ketika minus di tap
            itemView.findViewById<ImageView>(R.id.ic_minus)
                .setOnClickListener {
                    Log.d("sini", "sana")
                    // jika jumlah pada urutan ke "adapterPosition" lebih dari 0
                    if (listModel[adapterPosition].jumlah > 0) {
                        // mengurangi pada urutan ke "adapterPosition"
                        listModel[adapterPosition].jumlah = listModel[adapterPosition].jumlah - 1
                        notifyItemChanged(adapterPosition)

                        callback.ketikaJumlahBerubah(adapterPosition)
                    }
                }

            // atur ketika plus di tap
            itemView.findViewById<ImageView>(R.id.ic_plus)
                .setOnClickListener {
                    // menambahkan pada urutan ke "adapterPosition"
                    listModel[adapterPosition].jumlah = listModel[adapterPosition].jumlah + 1
                    notifyItemChanged(adapterPosition)

                    callback.ketikaJumlahBerubah(adapterPosition)
                }
        }
        // saat menampilkan view
        // mengubah tampilan nama, deskripsi, harga, dll
        fun onBind(model: MakananModel) {
            textNama.text = model.nama
            textDeskripsi.text = model.deskripsi
            textHarga.text = "Rp ${model.harga}"
            imageMakanan.setImageResource(model.gambar)
            textTotal.text = "${model.jumlah}"
        }
    }

    interface AdapterMakananCallback {
        fun ketikaJumlahBerubah(position: Int)
    }
}


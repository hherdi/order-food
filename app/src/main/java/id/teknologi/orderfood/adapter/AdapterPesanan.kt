package id.teknologi.orderfood.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import id.teknologi.orderfood.R
import id.teknologi.orderfood.model.MakananModel
import kotlinx.android.synthetic.main.item_summary_pesanan.view.*

class AdapterPesanan(val context : Context, val listModel : ArrayList<MakananModel>)
    : RecyclerView.Adapter<AdapterPesanan.ViewHolderMakanan>() {


    // inisiasi view holder
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolderMakanan {
        val inflater = LayoutInflater.from(context)
        // view yang akan digunakan adalah item_makanan
        val view = inflater.inflate(R.layout.item_summary_pesanan, viewGroup, false)

        return ViewHolderMakanan(view)
    }

    // jumlah dari item yang akan ditampilkan
    override fun getItemCount(): Int {
        return listModel.size
    }

    // mengatur tampilan view pada urutan ke-idx
    override fun onBindViewHolder(viewHolder: ViewHolderMakanan, idx: Int) {
        viewHolder.onBind(listModel[idx])
    }

    inner class ViewHolderMakanan(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val textNama : TextView
        val textJumlah : TextView
        val textHarga : TextView

        init {
            // binding / menghubungkan variable dengan view item_makanan.xml
            textNama = itemView.tv_nama_pesanan
            textHarga = itemView.tv_harga_pesanan
            textJumlah = itemView.tv_jumlah_pesanan
        }

        // saat menampilkan view
        // mengubah tampilan nama, deskripsi, harga, dll
        fun onBind(model: MakananModel) {
            textNama.text = model.nama
            textJumlah.text = "@ ${model.jumlah}"
            textHarga.text = "Rp ${model.harga}"
        }
    }
}


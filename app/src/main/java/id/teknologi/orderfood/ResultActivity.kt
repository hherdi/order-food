package id.teknologi.orderfood

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.widget.Adapter
import id.teknologi.orderfood.adapter.AdapterMakanan
import id.teknologi.orderfood.adapter.AdapterPesanan
import id.teknologi.orderfood.model.MakananModel
import kotlinx.android.synthetic.main.activity_result.*
import android.content.Intent



class ResultActivity : AppCompatActivity(), AdapterMakanan.AdapterMakananCallback {

    lateinit var adapterPesanan: AdapterPesanan
    var totalHarga = 0L

    lateinit var listMakanan : ArrayList<MakananModel>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_result)

        val listPesanan = intent.getSerializableExtra("pesanan") as? ArrayList<MakananModel>

        listPesanan?.let {
            listMakanan = it
            // lihat logcat untuk melihat jumlah size makanan
            Log.d("teknologiid", "makanan terpesan" + it.size)

            kalkulasiTotal()
        }

        // set recycler adapter untuk yg bagian atas
        setupRecyclerAdapterMakanan()
        // set recycler adapter untuk yg bagian bawah
        setupRecyclerAdapterPesanan()

        btn_back.setOnClickListener {
            onBackPressed()
        }
        btn_tambah.setOnClickListener {
            onBackPressed()
        }

        btn_order.setOnClickListener {
            var pesananString = ""
            listMakanan.forEach {
                pesananString = pesananString + "${it.nama} @${it.jumlah} \n"
            }
            pesananString = pesananString + "--------------\n\nTotal Harga : Rp ${totalHarga}\n\n"
            pesananString = pesananString + "< Masukkan Alamat Pengiriman Makanan >"

            Log.d("Teknologiid", pesananString)

            val intent = Intent(Intent.ACTION_SEND)
            intent.putExtra(Intent.EXTRA_SUBJECT, "Pesan makanan")
            intent.putExtra(Intent.EXTRA_EMAIL, arrayOf<String>("admin@teknologi.id"))
            intent.putExtra(Intent.EXTRA_TEXT, pesananString)
            intent.setType("text/plain");

            startActivity(Intent.createChooser(intent, "Send Email"));
        }
    }

    fun setupRecyclerAdapterMakanan () {
        // inisiasi adapter
        val adapterMakanan = AdapterMakanan(this, listMakanan, this)

        // memasang adapter makanan ke recyclerview
        rv_list_makanan.adapter = adapterMakanan

        // menentukan recycler view layout manager
        val linearLayoutManager = LinearLayoutManager(
            this, LinearLayoutManager.VERTICAL, false)
        rv_list_makanan.layoutManager = linearLayoutManager

        rv_list_makanan.isNestedScrollingEnabled = true
    }

    fun setupRecyclerAdapterPesanan () {
        // inisiasi adapter
        adapterPesanan = AdapterPesanan(this, listMakanan)

        // memasang adapter makanan ke recyclerview
        rv_rangkuman_pesanan.adapter = adapterPesanan

        // menentukan recycler view layout manager
        val linearLayoutManager = LinearLayoutManager(
            this, LinearLayoutManager.VERTICAL, false)
        rv_rangkuman_pesanan.layoutManager = linearLayoutManager

        rv_rangkuman_pesanan.isNestedScrollingEnabled = true
    }

    override fun ketikaJumlahBerubah(position: Int) {
        adapterPesanan.notifyItemChanged(position)

        kalkulasiTotal()
    }

    fun kalkulasiTotal() {
        var jumlah = 0
        var totalHarga = 0L
        for (makanan in listMakanan) {
            if (makanan.jumlah > 0) {
                jumlah = jumlah + 1
            }
            totalHarga = totalHarga + (makanan.jumlah * makanan.harga)
        }
        this.totalHarga = totalHarga
        tv_total_pesanan.text = "Rp ${totalHarga}"
        tv_total_pesanan_big.text = "Rp ${totalHarga}"
    }
}